//
//  ViewController.swift
//  DrawBox
//
//  Created by 1 on 30.08.2018.
//  Copyright © 2018 web-academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let size: Int = 32
    let clearLine: Int = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // drawBox(x: 20, y: 50)
        // drawLineBox(3)
        // drawManyLineBox(3, startX: 50)
        // drawCentralPyramid(5)
        // drawCentralBox(8)
        // let color = UIColor.red.cgColor
        // drawCircle (centerX: 100, centerY: 100, radius: 30, color: color)
        drawCircleTarget(5)
    }
    
    func drawCircleTarget (_ count:Int) {
        var color = UIColor.red.cgColor
        for i in 0..<count {
            let radius = (count-i)*size
            if color == UIColor.red.cgColor {
                color = UIColor.darkGray.cgColor
            } else {
                color = UIColor.red.cgColor
            }
            drawCircle (centerX: 100, centerY: 100, radius: radius, color: color)
        }
    }
    
    func drawCircle (centerX: Int, centerY: Int, radius: Int, color: CGColor) {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: centerX,y: centerY), radius: CGFloat(radius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = color
        view.layer.addSublayer(shapeLayer)
    }
    
    func drawCentralBox(_ count: Int) {
        let centralX = 120
        let centralY = 200
        let size=30
        var color=UIColor.darkGray
        for i in 0..<count {
            let resize = (count-i)*size
            let x = centralX - Int(resize/2)
            let y = centralY - Int(resize/2)
            let rect = CGRect.init(x: x, y: y, width: resize, height: resize)
            let box=UIView.init(frame: rect)
            if color == UIColor.green {
                color = UIColor.darkGray
            } else {
                color = UIColor.green
            }
            box.backgroundColor = color
            view.addSubview(box)
        }
    }
    
    func drawCentralPyramid (_ count: Int) {
        var startY=20
        var startX=150
        for i in 0..<count {
            startY += size + clearLine
            startX -= Int( (size + clearLine)/2 )
            drawLineBox(i+1, lineY: startY, lineX: startX)
        }
    }
    
    func drawManyLineBox (_ count: Int, startX: Int = 20 ) {
        var startY = 40
        for i in 0..<count {
            startY += size + clearLine
            drawLineBox(i+1, lineY: startY, lineX: startX)
        }
    }
    
    func drawLineBox (_ count: Int, lineY: Int = 50, lineX: Int = 20) {
        var x = 0
        for _ in 0..<count {
            x += size + clearLine
            drawBox(x: lineX + x, y: lineY)
        }
    }
    
    func drawBox(x: Int, y: Int) {
        let rect = CGRect.init(x: x, y: y, width: size, height: size)
        let box=UIView.init(frame: rect)
        box.backgroundColor = UIColor.darkGray
        view.addSubview(box)
    }
    
}

